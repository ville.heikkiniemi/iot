-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 28, 2023 at 04:13 PM
-- Server version: 10.3.38-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `litih501_raspberry`
--

-- --------------------------------------------------------

--
-- Table structure for table `Ohjaukset`
--

CREATE TABLE `Ohjaukset` (
  `id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `username` varchar(50) NOT NULL,
  `ip` char(15) NOT NULL,
  `time` datetime DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `Ohjaukset`
--

INSERT INTO `Ohjaukset` (`id`, `state`, `username`, `ip`, `time`) VALUES
(44, 1, 'XXX', '87.95.59.87', '2023-03-18 13:38:06'),
(45, 1, 'XXX', '213.186.232.162', '2023-03-21 12:40:26'),
(46, 1, 'XXX', '176.93.231.96', '2023-03-23 13:46:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Ohjaukset`
--
ALTER TABLE `Ohjaukset`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Ohjaukset`
--
ALTER TABLE `Ohjaukset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
