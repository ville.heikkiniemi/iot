-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 20, 2023 at 05:54 PM
-- Server version: 10.3.38-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `litih501_raspberry`
--

-- --------------------------------------------------------

--
-- Table structure for table `Infot`
--

CREATE TABLE `Infot` (
  `Koodi` varchar(3) NOT NULL,
  `Maarittely` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `Infot`
--

INSERT INTO `Infot` (`Koodi`, `Maarittely`) VALUES
('61', 'Jalankulkijoiden punainen p&auml;&auml;lle'),
('50', 'Jalankulkijoiden vihre&auml; pois'),
('11', 'Portti auki'),
('10', 'Portti kiinni'),
('31', 'Ajoneuvojen keltainen p&auml;&auml;lle'),
('30', 'Ajoneuvojen keltainen pois'),
('40', 'Ajoneuvojen punainen pois'),
('41', 'Ajoneuvojen punainen p&auml;&auml;lle'),
('60', 'Jalankulkijoiden punainen pois'),
('51', 'Jalankulkijoiden vihre&auml; p&auml;&auml;lle'),
('20', 'Ajoneuvojen vihre&auml; pois'),
('21', 'Ajoneuvojen vihre&auml; p&auml;&auml;lle');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Infot`
--
ALTER TABLE `Infot`
  ADD PRIMARY KEY (`Koodi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
