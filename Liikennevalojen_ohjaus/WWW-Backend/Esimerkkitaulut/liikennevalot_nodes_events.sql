-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 23, 2023 at 02:18 PM
-- Server version: 10.3.38-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `litih501_raspberry`
--

-- --------------------------------------------------------

--
-- Table structure for table `liikennevalot_nodes_events`
--

CREATE TABLE `liikennevalot_nodes_events` (
  `id` int(11) NOT NULL,
  `node_id` char(4) NOT NULL,
  `event` char(2) NOT NULL,
  `time` int(11) DEFAULT NULL,
  `value` char(8) DEFAULT NULL,
  `sensor` char(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `liikennevalot_nodes_events`
--

INSERT INTO `liikennevalot_nodes_events` (`id`, `node_id`, `event`, `time`, `value`, `sensor`) VALUES
(70477, 'Home', 'I', 1679395240, '10', ''),
(70478, 'Home', 'I', 1679395240, '60', ''),
(70479, 'Home', 'I', 1679395241, '51', ''),
(70480, 'Home', 'C', 1679395241, '10', ''),
(70481, 'Home', 'C', 1679462190, '01', 'U'),
(70482, 'Home', 'I', 1679462190, '61', ''),
(70483, 'Home', 'I', 1679462190, '50', ''),
(70484, 'Home', 'I', 1679462191, '11', ''),
(70485, 'Home', 'I', 1679462192, '31', ''),
(70486, 'Home', 'I', 1679462193, '40', ''),
(70487, 'Home', 'I', 1679462194, '30', ''),
(70488, 'Home', 'I', 1679462194, '21', ''),
(70489, 'Home', 'C', 1679462194, '00', ''),
(70490, 'Home', 'C', 1679462202, '11', ''),
(70491, 'Home', 'I', 1679462202, '20', ''),
(70492, 'Home', 'I', 1679462202, '31', ''),
(70493, 'Home', 'I', 1679462205, '30', ''),
(70494, 'Home', 'I', 1679462205, '41', ''),
(70495, 'Home', 'I', 1679462206, '10', ''),
(70496, 'Home', 'I', 1679462206, '60', ''),
(70497, 'Home', 'I', 1679462206, '51', ''),
(70498, 'Home', 'C', 1679462207, '10', ''),
(70499, 'Home', 'C', 1679571961, '01', 'S'),
(70500, 'Home', 'I', 1679571962, '61', ''),
(70501, 'Home', 'I', 1679571962, '50', ''),
(70502, 'Home', 'I', 1679571963, '11', ''),
(70503, 'Home', 'I', 1679571964, '31', ''),
(70504, 'Home', 'I', 1679571965, '40', ''),
(70505, 'Home', 'I', 1679571965, '30', ''),
(70506, 'Home', 'I', 1679571966, '21', ''),
(70507, 'Home', 'C', 1679571966, '00', ''),
(70508, 'Home', 'C', 1679571970, '11', ''),
(70509, 'Home', 'I', 1679571970, '20', ''),
(70510, 'Home', 'I', 1679571970, '31', ''),
(70511, 'Home', 'I', 1679571973, '30', ''),
(70512, 'Home', 'I', 1679571973, '41', ''),
(70513, 'Home', 'I', 1679571974, '10', ''),
(70514, 'Home', 'I', 1679571974, '60', ''),
(70515, 'Home', 'I', 1679571974, '51', ''),
(70516, 'Home', 'C', 1679571975, '10', '');

--
-- Triggers `liikennevalot_nodes_events`
--
DELIMITER $$
CREATE TRIGGER `NewEventAfter` AFTER INSERT ON `liikennevalot_nodes_events` FOR EACH ROW BEGIN
INSERT INTO `NewEvents` (`id`, `eventId`, `event`, `value`, `time`) VALUES (NULL, NEW.id, NEW.event, NEW.value, NEW.time);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `NewEventBefore` BEFORE INSERT ON `liikennevalot_nodes_events` FOR EACH ROW BEGIN
IF NEW.event LIKE 'C' AND NEW.value LIKE '01' THEN
DELETE FROM `NewEvents`;
END IF;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `liikennevalot_nodes_events`
--
ALTER TABLE `liikennevalot_nodes_events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `liikennevalot_nodes_events`
--
ALTER TABLE `liikennevalot_nodes_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70517;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
