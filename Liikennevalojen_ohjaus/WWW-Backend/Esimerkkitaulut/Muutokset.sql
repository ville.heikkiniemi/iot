-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 20, 2023 at 05:59 PM
-- Server version: 10.3.38-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `litih501_raspberry`
--

-- --------------------------------------------------------

--
-- Table structure for table `Muutokset`
--

CREATE TABLE `Muutokset` (
  `Koodi` varchar(3) NOT NULL,
  `Maarittely` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `Muutokset`
--

INSERT INTO `Muutokset` (`Koodi`, `Maarittely`) VALUES
('11', 'Estet&auml;&auml;n ajoneuvot. P&auml;&auml;stet&auml;&auml;n jalankulkijat.'),
('10', 'Ajoneuvoilla punainen. Portti kiinni. Jalankulkijoilla vihre&auml;.'),
('00', 'Ajoneuvoilla vihre&auml;. Portti auki. Jalankulkijoilla punainen.'),
('01', 'P&auml;&auml;stet&auml;&auml;n ajoneuvot. Estet&auml;&auml;n jalankulkijat.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Muutokset`
--
ALTER TABLE `Muutokset`
  ADD PRIMARY KEY (`Koodi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
