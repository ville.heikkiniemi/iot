-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 23, 2023 at 02:26 PM
-- Server version: 10.3.38-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `litih501_raspberry`
--

-- --------------------------------------------------------

--
-- Table structure for table `NewEvents`
--

CREATE TABLE `NewEvents` (
  `id` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `event` char(2) NOT NULL,
  `value` char(8) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `NewEvents`
--

INSERT INTO `NewEvents` (`id`, `eventId`, `event`, `value`, `time`) VALUES
(33372, 70499, 'C', '01', 1679571961),
(33373, 70500, 'I', '61', 1679571962),
(33374, 70501, 'I', '50', 1679571962),
(33375, 70502, 'I', '11', 1679571963),
(33376, 70503, 'I', '31', 1679571964),
(33377, 70504, 'I', '40', 1679571965),
(33378, 70505, 'I', '30', 1679571965),
(33379, 70506, 'I', '21', 1679571966),
(33380, 70507, 'C', '00', 1679571966),
(33381, 70508, 'C', '11', 1679571970),
(33382, 70509, 'I', '20', 1679571970),
(33383, 70510, 'I', '31', 1679571970),
(33384, 70511, 'I', '30', 1679571973),
(33385, 70512, 'I', '41', 1679571973),
(33386, 70513, 'I', '10', 1679571974),
(33387, 70514, 'I', '60', 1679571974),
(33388, 70515, 'I', '51', 1679571974),
(33389, 70516, 'C', '10', 1679571975);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `NewEvents`
--
ALTER TABLE `NewEvents`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `NewEvents`
--
ALTER TABLE `NewEvents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33390;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
