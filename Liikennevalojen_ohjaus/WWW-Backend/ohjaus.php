<?php
/*
DROP TABLE IF EXISTS `Ohjaukset`;
CREATE TABLE `Ohjaukset` (
 `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `state` BOOLEAN NOT NULL,
 `username` varchar(50) NOT NULL,
 `ip` char(15) NOT NULL,
 `time` datetime DEFAULT CURRENT_TIMESTAMP
);
*/
date_default_timezone_set("Europe/Helsinki");
session_start();
require('session.php');
require_once('db.php');
$conn = OpenCon();
// Varmistetaan, että ohjaus tapahtuu etusivun OHJAA-painikkeella
if ( isset($_POST['hash']) && isset($_POST['oKey']) && $omatunnus != "0" ) {
	if ( hash_equals(hash_hmac('sha256', $_POST['oKey'], $key) , $_POST['hash']) == true ) {
		// Kirjoitetaan ohjaustieto tietokantaan
		$sql = "INSERT INTO Ohjaukset (state, username, ip) VALUES (TRUE, '" . $omatunnus . "', '" . $_SERVER['REMOTE_ADDR'] ."')";
		$conn->query($sql);
	}
}
CloseCon($conn);
?>