<?php
/*
Tehdään tietokanta, johon uusimman prosessin tietueet tallennetaan triggerien avulla
------------------
DROP TABLE IF EXISTS `NewEvents`;
CREATE TABLE `NewEvents` (
 `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `eventId` int(11) NOT NULL,
 `event` char(2) NOT NULL,
 `value` char(8) NOT NULL,
 `time` int(11) NOT NULL
)

Tehdään tietokantaan triggerit
------------------
DROP TRIGGER IF EXISTS `NewEventBefore`;
DELIMITER //
CREATE TRIGGER `NewEventBefore` BEFORE INSERT ON `liikennevalot_nodes_events`
FOR EACH ROW
BEGIN
IF NEW.event LIKE 'C' AND NEW.value LIKE '01' THEN
DELETE FROM `NewEvents`;
END IF;
END;//
DELIMITER ;


DROP TRIGGER IF EXISTS `NewEventAfter`;
DELIMITER //
CREATE TRIGGER `NewEventAfter` AFTER INSERT ON `liikennevalot_nodes_events`
FOR EACH ROW
BEGIN
INSERT INTO `NewEvents` (`id`, `eventId`, `event`, `value`, `time`) VALUES (NULL, NEW.id, NEW.event, NEW.value, NEW.time);
END;//
DELIMITER ;
*/
date_default_timezone_set("Europe/Helsinki");
header("Cache-Control: no-cache");
header("Content-Type: text/event-stream");
$counter = 0;
require_once('db.php');
$conn = OpenCon();

// Haetaan viimeisimmän prosessin viimeinen Id
$sqlTapahtumat = "SELECT eventId FROM NewEvents ORDER BY id DESC LIMIT 1";
$tapahtumatRes = $conn->query($sqlTapahtumat);
if ($tapahtumatRes->num_rows > 0) {
	while($row = $tapahtumatRes->fetch_assoc()) {
		$lastIdHistory = $row["eventId"];
	}
} else {
	$lastIdHistory = '0';
}

// Infot
$sqlInfot = "SELECT * FROM Infot";
$infotRes = $conn->query($sqlInfot);
for ($infot = array (); $row = $infotRes->fetch_assoc(); $infot[] = $row);

// Muutokset
$sqlMuutokset = "SELECT * FROM Muutokset";
$muutoksetRes = $conn->query($sqlMuutokset);
for ($muutokset = array (); $row = $muutoksetRes->fetch_assoc(); $muutokset[] = $row);


while(true){
	// Päivitetään viimeisimmän prosessitietueen id
	$tapahtumat = $conn->query($sqlTapahtumat);
	if ($tapahtumat->num_rows > 0) {
		while($row = $tapahtumat->fetch_assoc()) {
			$lastId = $row["eventId"];
		}
	} else {
		$lastId = '0';
	}
	// Jos päivitetty id on uudempi, tulostetaan uudet prosessitietueet muotoiluineen
	if ( $lastIdHistory < $lastId ) {
		// Tulostetaan id-järjestyksessä lähtien historiatiedosta seuraavasta
		while($lastIdHistory < $lastId) {
			// Kootaan tuloste
			$sqlTapahtuma = "SELECT eventId, event, value, time FROM NewEvents WHERE eventId='" . (intval($lastIdHistory)+1) . "'";
			$tapahtumaRes = $conn->query($sqlTapahtuma);
			for ($tapahtuma = array (); $row = $tapahtumaRes->fetch_assoc(); $tapahtuma[] = $row);
			if ($tapahtuma[0]['event'] == 'I' ) {
				$data = "<tr class='Info' id='" . $tapahtuma[0]['eventId'] . "'>";
				$data .= "<td>" . date("d.m.Y H:i:s", $tapahtuma[0]['time']) . "</td>";
				foreach ($infot as $info){ 
					if ($info['Koodi'] == $tapahtuma[0]['value']) {
						$data .= "<td>" . $info['Maarittely'] . "</td></tr>";
					}
				}
			} else {
				$data = "<tr class='Muutos' id='" . $tapahtuma[0]['eventId'] . "'>";
				$data .= "<td>" . date("d.m.Y H:i:s", $tapahtuma[0]['time']) . "</td>";
				foreach ($muutokset as $muutos){ 
					if ($muutos['Koodi'] == $tapahtuma[0]['value']) {
						$data .= "<td>" . $muutos['Maarittely'] . "</td></tr>";
					}
				}
			}
			// Päivitetään historiatieto, tulostetaan ja tyhjennetään puskuri
			$lastIdHistory = $tapahtuma[0]['eventId'];
			echo "data: " . $data . "\n\n";
			clearstatcache();
			ob_end_flush();
			flush();
		}
		$counter=10;
	}
	// Yhteyden ylläpito
	if (!$counter) {
		$counter=20;
		echo "data: " . intval($lastId) . "\n\n";
		clearstatcache();
		ob_end_flush();
		flush();
	}
	// Pienennetään laskuria ja odotetaan hetki
	$counter--;
	usleep(500000);
}
CloseCon($conn);
?>