<?php
/*
CREATE TABLE `liikennevalot_nodes_events` (
 `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `node_id` char(4) NOT NULL,
 `event` char(2) NOT NULL,
 `time` int(11) DEFAULT NULL,
 `value` char(8) DEFAULT NULL
)

DROP TABLE IF EXISTS `Infot`;
CREATE TABLE `Infot` (
 `Koodi` VARCHAR(3) PRIMARY KEY,
 `Maarittely` VARCHAR(50) NOT NULL
)

DROP TABLE IF EXISTS `Muutokset`;
CREATE TABLE `Muutokset` (
 `Koodi` VARCHAR(3) PRIMARY KEY,
 `Maarittely` VARCHAR(100) NOT NULL
)
*/

date_default_timezone_set('Europe/Helsinki');
require_once('db.php');
$conn = OpenCon();

// Päätietokanta voidaan hakea tietue kerrallaan tai oletuksena viimeiset sata tietuetta lajiteltuna uusin ensin.
if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['rowId'])) {
		if ( $stmt = $conn->prepare("SELECT t.id AS ID, t.time AS Aika, m.Maarittely AS MMaarittely, i.Maarittely AS IMaarittely, SUBSTR(t.value,-1) AS Value FROM liikennevalot_nodes_events t LEFT JOIN Muutokset m ON t.value = m.Koodi AND t.event = 'C' LEFT JOIN Infot i ON t.value = i.Koodi AND t.event = 'I' WHERE ID=?;") ) {
		$stmt->bind_param("s", $_GET["rowId"]);
		$stmt->execute();
		$haku = $stmt->get_result();
		$stmt->close();
		while($tietue = $haku->fetch_assoc()){
			if (empty($tietue['MMaarittely'])) {
				printf("<tr class='Info' id='%s'>", $tietue['ID']);
				printf("<td>%s</td>", date("d.m.Y H:i:s", $tietue['Aika']));
				printf("<td>%s</td></tr>", $tietue['IMaarittely']);
			}
			if (empty($tietue['IMaarittely']) && $tietue['Value']=='0') {
				printf("<tr class='Muutos' id='%s'>", $tietue['ID']);
				printf("<td><span>-</span> %s</td>", date("d.m.Y H:i:s", $tietue['Aika']));
				printf("<td>%s</td></tr>", $tietue['MMaarittely']);
			}
			if (empty($tietue['IMaarittely']) && $tietue['Value']=='1') {
				printf("<tr class='Muutos' id='%s'>", $tietue['ID']);
				printf("<td>%s</td>", date("d.m.Y H:i:s", $tietue['Aika']));
				printf("<td>%s</td></tr>", $tietue['MMaarittely']);
			}
		}
	}
} else {
	if ( $stmt = $conn->prepare("SELECT t.id AS ID, t.time AS Aika, m.Maarittely AS MMaarittely, i.Maarittely AS IMaarittely, SUBSTR(t.value,-1) AS Value FROM liikennevalot_nodes_events t LEFT JOIN Muutokset m ON t.value = m.Koodi AND t.event = 'C' LEFT JOIN Infot i ON t.value = i.Koodi AND t.event = 'I' ORDER BY t.id DESC LIMIT 300;") ) {
		$stmt->execute();
		$haku = $stmt->get_result();
		$stmt->close();
		while($tietue = $haku->fetch_assoc()){
			if (empty($tietue['MMaarittely'])) {
				printf("<tr class='Info' id='%s'>", $tietue['ID']);
				printf("<td>%s</td>", date("d.m.Y H:i:s", $tietue['Aika']));
				printf("<td>%s</td></tr>", $tietue['IMaarittely']);
			}
			if (empty($tietue['IMaarittely']) && $tietue['Value']=='0') {
				printf("<tr class='Muutos' id='%s'>", $tietue['ID']);
				printf("<td><span>-</span> %s</td>", date("d.m.Y H:i:s", $tietue['Aika']));
				printf("<td>%s</td></tr>", $tietue['MMaarittely']);
			}
			if (empty($tietue['IMaarittely']) && $tietue['Value']=='1') {
				printf("<tr class='Muutos MuutosS' id='%s'>", $tietue['ID']);
				printf("<td>%s</td>", date("d.m.Y H:i:s", $tietue['Aika']));
				printf("<td>%s</td></tr>", $tietue['MMaarittely']);
			}
		}
	}
}
CloseCon($conn);
?>