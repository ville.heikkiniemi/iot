<?php
/*
DROP TABLE IF EXISTS `Ohjaukset`;
CREATE TABLE `Ohjaukset` (
 `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `state` BOOLEAN NOT NULL,
 `username` varchar(50) NOT NULL,
 `ip` char(15) NOT NULL,
 `time` datetime DEFAULT CURRENT_TIMESTAMP
);
*/
date_default_timezone_set("Europe/Helsinki");
header("Cache-Control: no-cache");
header("Content-Type: text/event-stream");
require('session.php');
require_once('db.php');
$conn = OpenCon();
$counter = 0;
$data = array('hash'=>'','time'=>'','state'=>'');

// Allekirjoittamiseen käytetty avain
$sKey = 'e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683';

// Haetaan viimeisimmän prosessin viimeinen Id
$sqlOhjaukset = "SELECT id FROM Ohjaukset ORDER BY id DESC LIMIT 1";
$ohjaukset = $conn->query($sqlOhjaukset);
if ($ohjaukset->num_rows > 0) {
	while($tietue = $ohjaukset->fetch_assoc()) {
		$lastIdHistory = $tietue["id"];
	}
} else {
	$lastIdHistory = '0';
}

while(true){
	// Päivitetään viimeisimmän prosessitietueen id
	$ohjaukset = $conn->query($sqlOhjaukset);
	if ($ohjaukset->num_rows > 0) {
		while($tietue = $ohjaukset->fetch_assoc()) {
			$lastId = $tietue["id"];
		}
	} else {
		$lastId = '0';
	}
	// Täydennetään kaksi tulosteena olevan taulukon alkioista > aikaleima ja allekirjoitus
	$timestamp = time();
	$data['hash'] = hash_hmac('sha256', $timestamp , $sKey);
	$data['time'] = $timestamp;
	
	// Täydennetään tulosteen viimeinen alkio, lähetetään tuloste, tyhjennetään puskuri ja päivitetään historiatietoa
	if ( $lastIdHistory < $lastId ) {
		$data['state'] = 1;
		echo "data: " . json_encode($data) . "\n\n";
		clearstatcache();
		ob_end_flush();
		flush();
		usleep(500000);
		$counter=1;
		$lastIdHistory = $lastId;
	}
	// Oletustuloste, kun tilaa ei ohjattu web-liittymän kautta.
	if (!$counter) {
		$data['state'] = 0;
		echo "data: " . json_encode($data) . "\n\n";
		clearstatcache();
		ob_end_flush();
		flush();
		$counter=20;
	}
	$counter--;
	usleep(500000);
}
CloseCon($conn);
?>