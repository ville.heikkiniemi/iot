<?php
/* Luotu MySQL-tietokanta ja -taulut
DROP TABLE IF EXISTS `liikennevalot_nodes_events`;
CREATE TABLE liikennevalot_nodes_events (
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	node_id char(4) NOT NULL,
	event char(2) NOT NULL,
	time int(11),
	value char(8),
	sensor char(2)
);

DROP TABLE IF EXISTS `nodes`;
CREATE TABLE `nodes` (
 `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `nodeId` varchar(50) NOT NULL,
 `nodeKey` varchar(100) NOT NULL,
 `password` varchar(100) NOT NULL,
 `created_at` datetime DEFAULT CURRENT_TIMESTAMP
)
*/

date_default_timezone_set("Europe/Helsinki");
require_once('db.php');
// Maaritellaan POST-metodiin reagoiva lause
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	// POSTissa pitaa olla maariteltyna ja annettuna parametrit
	if ( !empty($_POST["nodeId"]) && !empty($_POST["password"]) && !empty($_POST["event"]) && !empty($_POST["value"]) && !empty($_POST["msgId"]) && !empty($_POST["time"]) ) {
		// Tarkistetaan aikaleima > pyritään estämäään viestin kaappaus ja monistaminen
		if ( (int)$_POST["time"]-time() < 3 && time()-(int)$_POST["time"] < 3 ) {
			// Avataan tietokantayhteys
			$conn = OpenCon();
			// Autentikoidaan ja autorisoidaan viesti.
			if ( $stmt = $conn->prepare("SELECT nodeKey, password FROM nodes WHERE nodeId=?") ) {
				$stmt->bind_param("s", $_POST["nodeId"]);
				$stmt->execute();
				$result = $stmt->get_result();
				$stmt->close();
				if ($result->num_rows > 0) {
					while ($row = $result->fetch_assoc()) {
						// Jos hash ja salasana täsmää, lisätään tietokantaan
						if ( hash_equals(hash_hmac('sha256', $_POST["time"] , $row['nodeKey']), $_POST["msgId"]) && password_verify($_POST["password"], $row['password']) ) {
							// Kirjoitetaan event tietokantaan
							if (isset($_POST["sensor"])) {
								$stmt = $conn->prepare("INSERT INTO liikennevalot_nodes_events (node_id, event, time, value, sensor) VALUES (?, ?, UNIX_TIMESTAMP(), ?, ?)");
								$stmt->bind_param("ssss", $_POST["nodeId"], $_POST["event"], $_POST["value"], $_POST["sensor"]);
							} else {
								$stmt = $conn->prepare("INSERT INTO liikennevalot_nodes_events (node_id, event, time, value) VALUES (?, ?, UNIX_TIMESTAMP(), ?)");
								$stmt->bind_param("sss", $_POST["nodeId"], $_POST["event"], $_POST["value"]);
							}
							if($stmt->execute()) { printf("OK"); }
							$stmt->close();
						}
					}
				}
			}
			// Suljetaan tietokantayhteys
			CloseCon($conn);
		}
	}
}
?>