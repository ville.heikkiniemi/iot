<?php
/*
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
 `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `username` varchar(50) NOT NULL,
 `password` varchar(100) NOT NULL,
 `created_at` datetime DEFAULT CURRENT_TIMESTAMP
)

DROP TABLE IF EXISTS `nodes`;
CREATE TABLE `nodes` (
 `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `nodeId` varchar(50) NOT NULL,
 `nodeKey` varchar(100) NOT NULL,
 `password` varchar(100) NOT NULL,
 `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) */
session_start();
require_once('session.php');
require_once('db.php');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Rekisteröidy</title>
<link rel="StyleSheet" href="register.css" type="text/css" />
<link rel="icon" href="data:image/png;base64,iVBORw0KGgo=" type="image/png">
</head>
<body>
<div id="registercenter">
<div id="header">
Rekisteröityminen
</div>
<?php
// lasketaan yhteyden hash ja tarkistetaan onko käynyt etusivulla
if ( ( hash_equals(hash_hmac('sha256', $_SESSION["id"], $key) , $_GET['skey']) == true ) && $_SESSION["index"] == true ) {
	// Maaritellaan POST-metodiin reagoiva lause
	if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['register'])) {
		// POSTissa pitaa olla maariteltyna ja annettuna parametrit
		if ( !empty($_POST["username"]) && !empty($_POST["password"]) && !empty($_POST["usertypelist"]) ) {
			// Avataan tietokantayhteys
			$conn = OpenCon();
			// Tarkistetaan duplikaatit käyttäjätunnuksista
			if ( $_POST["usertypelist"] == "user" ) {
				$stmt = $conn->prepare("SELECT username FROM users WHERE username=?");
				$stmt->bind_param("s", $_POST["username"]);
				$stmt->execute();
				$result = $stmt->get_result();
				$stmt->close();
				if ($result->num_rows > 0) {
					$register = False;
				} else {
					$register = True;
				}
			}
			// Tarkistetaan duplikaatit nodeista
			if ( $_POST["usertypelist"] == "node" ) {
				$stmt = $conn->prepare("SELECT nodeId FROM nodes WHERE nodeId=?");
				$stmt->bind_param("s", $_POST["username"]);
				$stmt->execute();
				$result = $stmt->get_result();
				$stmt->close();
				if ($result->num_rows > 0) {
					$register = False;
				} else {
					$register = True;
				}
			}
			// Asetetaan käyttäjätunnus ja salasana
			if ( $register && $_POST["usertypelist"] == "user" && $stmt = $conn->prepare("INSERT INTO users (username, password) VALUES (?, ?)") ) {
				$password = password_hash($_POST["password"], PASSWORD_BCRYPT);
				$stmt->bind_param("ss", $_POST["username"], $password);
				if($stmt->execute()) {
					$register = True;
				} else {
					$register = False;
				}
				$stmt->close();
			}
			// Asetetaan node_id ja salasana
			if ( $register && $_POST["usertypelist"] == "node" && $stmt = $conn->prepare("INSERT INTO nodes (nodeId, nodeKey, password) VALUES (?, ?, ?)") ) {
				$password = password_hash($_POST["password"], PASSWORD_BCRYPT);
				$stmt->bind_param("sss", $_POST["username"], hash('sha256', rand(1, 10)), $password);
				if($stmt->execute()) {
					$register = True;
				} else {
					$register = False;
				}
				$stmt->close();
			}
			// Onnistuiko rekisteröinti
			if ($register) {
				echo "<div id=\"okregister\">Rekisteröityminen onnistui!</div>";
				echo "<script> window.location.assign('login.php?skey=" . $_GET['skey'] . "'); </script>";
			} else {
				echo "<div id=\"failedregister\">Rekisteröityminen epäonnistui!</div>";
			}
		}
		// Suljetaan tietokantayhteys
		CloseCon($conn);
	}
	echo "<div class=\"registerform\"><form action=";
	echo htmlspecialchars($_SERVER["PHP_SELF"]) . "?skey=" . $_GET['skey'];
	echo " method=\"post\">
	<p><label for=\"username\"></label>
	<input type=\"text\" name=\"username\" placeholder=\"Tunnus\" id=\"username\" required size=\"50\"></p>
	<p><label for=\"password\"></label>
	<input type=\"password\" name=\"password\" placeholder=\"Salasana\" id=\"password\" required></p>
	<p><label for=\"usertype\"></label>
	<select id=\"users\" name=\"usertypelist\" required>
		<option disabled value=\"\" selected hidden>Käyttäjän tyyppi</option>
		<option value=\"user\">Käyttäjä</option>
		<option value=\"node\">Node</option>
	</select></p>
	<p><input type=\"submit\" name=\"register\" id=\"registerbutton\" value=\"Rekisteröidy\"></p>
	</form>";
} else {
	die();
}
?>
</div>
</body>
</html>
