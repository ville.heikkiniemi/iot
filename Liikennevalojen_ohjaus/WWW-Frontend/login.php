<?php
/*
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
 `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `username` varchar(50) NOT NULL,
 `password` varchar(100) NOT NULL,
 `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) */
session_start();
require_once('session.php');
require_once('db.php');
?>
<!DOCTYPE html>
<html>
<head>
<link rel="StyleSheet" href="login.css" type="text/css" />
<link rel="icon" href="data:image/png;base64,iVBORw0KGgo=" type="image/png">
<meta charset="utf-8">
<title>Kirjaudu</title>
</head>
<body>
<div id="logincenter">
<div id="header">
Kirjautuminen
</div>
<?php
// lasketaan yhteyden hash ja tarkistetaan onko käynyt etusivulla
if ( (hash_equals(hash_hmac('sha256', $_SESSION["id"], $key) , $_GET['skey']) == true ) && $_SESSION["index"] == true ) {
	// Maaritellaan POST-metodiin reagoiva lause
	if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['login'])) {
		// POSTissa pitaa olla maariteltyna ja annettuna parametrit
		if ( !empty($_POST["username"]) && !empty($_POST["password"]) ) {
			// Avataan tietokantayhteys
			$conn = OpenCon();
			// Tarkistetaan onko lahestyksessa annettu oikea tunnus ja salasana > Ohjataan tilanteen mukaan
			if ( $stmt = $conn->prepare("SELECT password FROM users WHERE username=?") ) {
				$stmt->bind_param("s", $_POST["username"]);
				$stmt->execute();
				$result = $stmt->get_result();
				$stmt->close();
				if ($result->num_rows > 0) {
					while ($row = $result->fetch_assoc()) {
						if ( password_verify($_POST["password"], $row['password']) ) {
							$_SESSION["username"] = $_POST["username"];
							echo "<div id=\"oklogin\">Kirjautuminen onnistui!</div>";
							echo "<script>window.location.assign('index.php'); </script>";
						} else {
							echo "<div id=\"failedlogin\">Kirjautuminen epäonnistui!</div>";
						}
					}
				} else {
					echo "<div id=\"failedlogin\">Kirjautuminen epäonnistui!</div>";
				}
			}
			// Suljetaan tietokantayhteys
			CloseCon($conn);
		}
	} elseif ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['register'])) {
		echo "<script> window.location.assign('register.php?skey=" . $_GET['skey'] . "'); </script>";
	}
	echo "<div class=\"loginform\"><form action=";
	echo htmlspecialchars($_SERVER["PHP_SELF"]) . "?skey=" . $_GET['skey'];
	echo " method=\"post\" novalidate >
	<p><label for=\"username\"></label>
	<input type=\"text\" name=\"username\" placeholder=\"Tunnus\" id=\"username\" required></p>
	<p><label for=\"password\"></label>
	<input type=\"password\" name=\"password\" placeholder=\"Salasana\" id=\"password\" required></p>
	<p><input type=\"submit\" name=\"login\" id=\"loginbutton\" value=\"Kirjaudu\">
	<input type=\"submit\" name=\"register\" id=\"registerbutton\" value=\"Rekisteröidy\"></p>
	</form></div>";
} else {
	die();
}
?>
</div>
</body>
</html>
