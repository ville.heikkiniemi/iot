<?php
session_start();
require_once('session.php');
unset($_SESSION["username"]);
unset($_SESSION["id"]);
unset($_SESSION["index"]);
session_destroy();
session_regenerate_id();
session_commit();
echo "<script> window.location.assign('index.php'); </script>";
?>
